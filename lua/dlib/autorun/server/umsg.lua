
-- No Copyright (/C/) 2020 DBotThePony

-- This is free and unencumbered software released into the public domain.

--  Anyone is free to copy, modify, publish, use, compile, sell, or
--  distribute this software, either in source code form or as a compiled
--  binary, for any purpose, commercial or non-commercial, and by any
--  means.

--  In jurisdictions that recognize copyright laws, the author or authors
--  of this software dedicate any and all copyright interest in the
--  software to the public domain. We make this dedication for the benefit
--  of the public at large and to the detriment of our heirs and
--  successors. We intend this dedication to be an overt act of
--  relinquishment in perpetuity of all present and future rights to this
--  software under copyright law.

--  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
--  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
--  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
--  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
--  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
--  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
--  OTHER DEALINGS IN THE SOFTWARE.

--  For more information, please refer to <http://unlicense.org/>

_G.umsg = _G.umsg or {}
local umsg = umsg
local net = DLib.net

local dlib_umsg = CreateConVar('dlib_umsg', '1', {}, 'Replace usermessages with net library. This does NOT require server restart!')

net.pool('dlib_umsg')

local current_msg, current_filter, current_buffer

local backup = {
	'Start',
	'Angle',
	'Bool',
	'Char',
	'Entity',
	'Float',
	'Long',
	'Short',
	'String',
	'Vector',
	'VectorNormal',
	'End',
}

for _, fnname in ipairs(backup) do
	umsg['DLib_' .. fnname] = umsg['DLib_' .. fnname] or umsg[fnname]
end

function umsg.Start(name, filter)
	if current_msg then
		net.Discard()
		DLib.MessageWarning('Starting new usermessage ', name, ' without finishing old one, ', current_msg, '! Discarding old message.')
	end

	net.Start('dlib_umsg')
	net.WriteString(name)
	current_msg, current_filter = name, filter or player.GetHumans()
end

function umsg.End(name, filter)
	if not current_msg then
		error('Not currently writting a message.')
	end

	net.Send(current_filter)

	current_msg, current_filter = nil, nil, nil
end

function umsg.Angle(...)
	net.WriteAngle(...)
end

function umsg.Bool(...)
	net.WriteBool(...)
end

-- source engine write numbers
-- directly from memory as little endians
function umsg.Char(value)
	net.AccessWriteBuffer():WriteByte(value)
end

function umsg.Entity(ent)
	umsg.Short(ent:EntIndex())
end

function umsg.Float(...)
	net.AccessWriteBuffer():WriteFloatLE(...)
end

function umsg.Long(value)
	net.AccessWriteBuffer():WriteInt32LE(value)
end

function umsg.Short(value)
	net.AccessWriteBuffer():WriteInt16LE(value)
end

local tracked = {}

function umsg.String(value)
	if not isstring(value) then
		local trace = debug.traceback('umsg.String: input is not a string! typeof ' .. type(value))

		if not tracked[trace] then
			MsgC('\n')
			ErrorNoHalt('This warning will be spit only once per traceback\n')
			ErrorNoHalt(trace)
			ErrorNoHalt('\nThis warning will be spit only once per traceback\n')
			MsgC('\n')

			tracked[trace] = true
		end

		value = tostring(value)
	end

	net.WriteString(value)
end

function umsg.Vector(...)
	net.WriteVector(...)
end

function umsg.VectorNormal(...)
	net.WriteNormal(...)
end

for _, fnname in ipairs(backup) do
	umsg['DLib_Rep_' .. fnname] = umsg[fnname]
end

local function update()
	if dlib_umsg:GetBool() then
		for _, fnname in ipairs(backup) do
			umsg[fnname] = umsg['DLib_Rep_' .. fnname]
		end
	else
		for _, fnname in ipairs(backup) do
			umsg[fnname] = umsg['DLib_' .. fnname]
		end
	end
end

update()
umsg.UpdateDLibStatus = update

function umsg.ForceUseDLib(status)
	if status then
		for _, fnname in ipairs(backup) do
			umsg[fnname] = umsg['DLib_Rep_' .. fnname]
		end
	else
		for _, fnname in ipairs(backup) do
			umsg[fnname] = umsg['DLib_' .. fnname]
		end
	end
end

cvars.AddChangeCallback('dlib_umsg', update, 'DLib')

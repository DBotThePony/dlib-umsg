
-- No Copyright (/C/) 2020 DBotThePony

-- This is free and unencumbered software released into the public domain.

--  Anyone is free to copy, modify, publish, use, compile, sell, or
--  distribute this software, either in source code form or as a compiled
--  binary, for any purpose, commercial or non-commercial, and by any
--  means.

--  In jurisdictions that recognize copyright laws, the author or authors
--  of this software dedicate any and all copyright interest in the
--  software to the public domain. We make this dedication for the benefit
--  of the public at large and to the detriment of our heirs and
--  successors. We intend this dedication to be an overt act of
--  relinquishment in perpetuity of all present and future rights to this
--  software under copyright law.

--  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
--  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
--  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
--  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
--  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
--  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
--  OTHER DEALINGS IN THE SOFTWARE.

--  For more information, please refer to <http://unlicense.org/>

local Net = DLib.Net

if SERVER then
	Net.Pool('dlib_umsg_nw')
end

local dlib_umsg_nw = CreateConVar('dlib_umsg_nw', '0', {FCVAR_REPLICATED}, 'Replace NWVars with DLib.Net variant. This does NOT require a server restart!')

local entMeta = FindMetaTable('Entity')

entMeta.GetNWInt_DLib = entMeta.GetNWInt_DLib or entMeta.GetNWInt
entMeta.GetNWFloat_DLib = entMeta.GetNWFloat_DLib or entMeta.GetNWFloat
entMeta.GetNWAngle_DLib = entMeta.GetNWAngle_DLib or entMeta.GetNWAngle
entMeta.GetNWBool_DLib = entMeta.GetNWBool_DLib or entMeta.GetNWBool
entMeta.GetNWEntity_DLib = entMeta.GetNWEntity_DLib or entMeta.GetNWEntity
entMeta.GetNWString_DLib = entMeta.GetNWString_DLib or entMeta.GetNWString
entMeta.GetNWVector_DLib = entMeta.GetNWVector_DLib or entMeta.GetNWVector

entMeta.SetNWInt_DLib = entMeta.SetNWInt_DLib or entMeta.SetNWInt
entMeta.SetNWFloat_DLib = entMeta.SetNWFloat_DLib or entMeta.SetNWFloat
entMeta.SetNWAngle_DLib = entMeta.SetNWAngle_DLib or entMeta.SetNWAngle
entMeta.SetNWBool_DLib = entMeta.SetNWBool_DLib or entMeta.SetNWBool
entMeta.SetNWEntity_DLib = entMeta.SetNWEntity_DLib or entMeta.SetNWEntity
entMeta.SetNWString_DLib = entMeta.SetNWString_DLib or entMeta.SetNWString
entMeta.SetNWVector_DLib = entMeta.SetNWVector_DLib or entMeta.SetNWVector

entMeta.GetNWVarTable_DLib = entMeta.GetNWVarTable_DLib or entMeta.GetNWVarTable

function entMeta:GetNWVarTable_DLib_Replacement()
	local input = self:DLibGetNWVarTable()
	local output = {}

	for _, data in next, input do
		for key, value in next, data do
			output[key] = value
		end
	end

	return output
end

local function enabled()
	if entMeta.GetNWInt == entMeta.DLibGetNWFloat then return end

	entMeta.GetNWInt = entMeta.DLibGetNWFloat -- gmod
	entMeta.GetNWFloat = entMeta.DLibGetNWFloat
	entMeta.GetNWAngle = entMeta.DLibGetNWAngle
	entMeta.GetNWBool = entMeta.DLibGetNWBool
	entMeta.GetNWEntity = entMeta.DLibGetNWEntity
	entMeta.GetNWString = entMeta.DLibGetNWString
	entMeta.GetNWVector = entMeta.DLibGetNWVector

	entMeta.SetNWInt = entMeta.DLibSetNWFloat -- gmod
	entMeta.SetNWFloat = entMeta.DLibSetNWFloat
	entMeta.SetNWAngle = entMeta.DLibSetNWAngle
	entMeta.SetNWBool = entMeta.DLibSetNWBool
	entMeta.SetNWEntity = entMeta.DLibSetNWEntity
	entMeta.SetNWString = entMeta.DLibSetNWString
	entMeta.SetNWVector = entMeta.DLibSetNWVector

	entMeta.GetNWVarTable = entMeta.GetNWVarTable_DLib_Replacement

	if SERVER then
		for _, ent in ipairs(ents.GetAll()) do
			for key, value in next, ent:GetNWVarTable() do
				if isnumber(value) then
					ent:DLibSetNWFloat(key, value)
				elseif isangle(value) then
					ent:DLibSetNWAngle(key, value)
				elseif isbool(value) then
					ent:DLibSetNWBool(key, value)
				elseif isentity(value) then
					ent:DLibSetNWEntity(key, value)
				elseif isstring(value) then
					ent:DLibSetNWString(key, value)
				elseif isvector(value) then
					ent:DLibSetNWVector(key, value)
				end
			end
		end
	end
end

local function disabled()
	if entMeta.GetNWInt == entMeta.GetNWInt_DLib then return end

	entMeta.GetNWInt = entMeta.GetNWInt_DLib -- gmod ...
	entMeta.GetNWFloat = entMeta.GetNWFloat_DLib
	entMeta.GetNWAngle = entMeta.GetNWAngle_DLib
	entMeta.GetNWBool = entMeta.GetNWBool_DLib
	entMeta.GetNWEntity = entMeta.GetNWEntity_DLib
	entMeta.GetNWString = entMeta.GetNWString_DLib
	entMeta.GetNWVector = entMeta.GetNWVector_DLib

	entMeta.SetNWInt = entMeta.SetNWInt_DLib -- gmod ...
	entMeta.SetNWFloat = entMeta.SetNWFloat_DLib
	entMeta.SetNWAngle = entMeta.SetNWAngle_DLib
	entMeta.SetNWBool = entMeta.SetNWBool_DLib
	entMeta.SetNWEntity = entMeta.SetNWEntity_DLib
	entMeta.SetNWString = entMeta.SetNWString_DLib
	entMeta.SetNWVector = entMeta.SetNWVector_DLib

	entMeta.GetNWVarTable = entMeta.GetNWVarTable_DLib

	if SERVER then
		for _, ent in ipairs(ents.GetAll()) do
			for key, value in next, ent:GetNWVarTable_DLib_Replacement() do
				if isnumber(value) then
					ent:SetNWInt_DLib(key, value)
					ent:SetNWFloat_DLib(key, value)
				elseif isangle(value) then
					ent:SetNWAngle_DLib(key, value)
				elseif isbool(value) then
					ent:SetNWBool_DLib(key, value)
				elseif isentity(value) then
					ent:SetNWEntity_DLib(key, value)
				elseif isstring(value) then
					ent:SetNWString_DLib(key, value)
				elseif isvector(value) then
					ent:SetNWVector_DLib(key, value)
				end
			end
		end
	end
end

if dlib_umsg_nw:GetBool() then
	enabled()
else
	disabled()
end

cvars.AddChangeCallback('dlib_umsg_nw', function()
	if dlib_umsg_nw:GetBool() then
		enabled()
	else
		disabled()
	end

	if SERVER then
		Net.Start('dlib_umsg_nw')
		Net.WriteBool(dlib_umsg_nw:GetBool())
		Net.Broadcast()
	end
end, 'DLib umsg')

if CLIENT then
	Net.Receive('dlib_umsg_nw', function()
		if Net.ReadBool() then
			enabled()
		else
			disabled()
		end
	end)
else
	hook.Add('PlayerAuthed', 'DLib nw', function(ply)
		Net.Start('dlib_umsg_nw')
		Net.WriteBool(dlib_umsg_nw:GetBool())
		Net.Send(ply)
	end)
end
